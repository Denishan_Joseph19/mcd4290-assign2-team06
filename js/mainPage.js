
var displayregion


// getting the regions array that we had saved under Regionlistnum ID(localstorage)
var regionDisplay= JSON.parse(localStorage.getItem("Regionlistnum"))


//checking whether the right array gets in
console.log(JSON.parse(localStorage.getItem("Regionlistnum")))

//defining the global variable displayregion, that's used to saved the region and the time it was added
displayregion=''

if(regionDisplay !== null) 
  {
    
          for(var i =0;  i< regionDisplay.length; i++ )
         //Defining displayregion message that gets updated in Index.html
           displayregion += '<li class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"  style="width: 350px; height: 80px; min-width: initial;color: #BCC6C; background:#2B547E "  onclick=viewRegion('+i+')>' +
                '<span class="mdl-list__item-primary-content">' +
                '<span>' +"Region Name:  "+regionDisplay[i]._name + '</span>' +
                '<span class="mdl-list__item-sub-title">' +'</br>'+"Selected Date/Time: "+ regionDisplay[i]._Datetime + '</span>' +
                '</span>' +
                '</li>'
    }
else
    {
        //If no Regions aren't there it will say No Saved Regions message
        displayregion ='<p class="Please Add Region By pressing Plus Button">No Saved Regions.</p>'
    }

  
  //Checking the message with console.log pannel
  console.log(displayregion)

  //Updates the saved region in index.html page
  var regionListRef = document.getElementById("regionsList");
  regionListRef.innerHTML= displayregion


  function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex); 
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}


//Mapbox access token to get it on page
mapboxgl.accessToken = 'pk.eyJ1IjoiZGVuaXNoYW45OSIsImEiOiJja2phOG9kZ3QwOGtiMzNtZWhsZ200cmN4In0.QwDeret4pB_wfZjRxI-50w';
let map = new mapboxgl.Map({
container: 'map', // container id
style: 'mapbox://styles/mapbox/streets-v11',
center: [79.861244,6.927079 ], 
zoom: 12
    });
    

//Here we introduced a bot that directs the user without getting confused
document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov, Hi</br><br></br>I'm Here to Guide You</br></br>Step By Step</br></br>First, Press the Current Location"

//Displaying the messages on recordRegion.html
var Refreshref = document.getElementById('Refresh')
var saveRef = document.getElementById('save')
var currentRef = document.getElementById('current')
var cornerRef = document.getElementById('corner')

//Buttons to turn on and off when needed
Refreshref.disabled=true
saveRef.disabled = false
currentRef.disabled = false
cornerRef.disabled= true


//Defining Variables used in this JavaScript file
var currentlocation =[]
var overall_corners = []
var RegionNames= new RegionList()



//This function marks the current location of the user
function currentloc()
{
  //Here the bot will Update It's Message
  document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov</br><br>Fantastic! </br>now select your current location by <br></br>clicking on the map"  
    
    //This specific function was defined by mapbox api
     map.on('click', function (e) 
   {
        //getting lng and lat values
      var lnglatObject = JSON.parse(JSON.stringify(e.lngLat.wrap()))
       // adding currentlocation

       currentlocation.push(lnglatObject.lng)
        currentlocation.push(lnglatObject.lat)
      Refreshref.disabled=false
   });

   //Defining currentlocation Array as null
   currentlocation=[]
    
    map.on('click', function (e) 
   {

      //updating the message of Bot  
      document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov</br><br>welldone!! you are on track,</br></br>if you have selected the wrong </br></br>location press the refresh button</br></br></br>else, click the cornerlocation button to </br>proceed" 
   
      //This centers the map
     if(currentlocation>0)
      {
        map.flyTo(
        {
        center: currentlocation
        })
      }

       //Here this var marker will update the marking point according to the location the user selects
        var marker = new mapboxgl.Marker
        ({
          color: "#0000FF",
          draggable: true
         })
        .setLngLat([currentlocation[0], currentlocation[1]])
        .addTo(map);
             
        let popup = new mapboxgl.Popup({offset:30})
        .setLngLat([currentlocation[0], currentlocation[1]])
        .setHTML('Lng:'+currentlocation[0].toFixed(2)+' Lat:'+currentlocation[1].toFixed(2))
        .addTo(map) 
   
         currentRef.disabled = true
         cornerRef.disabled= false
    })

    
}
    

 

 //This function adds corner locations
function cornerloc()
{  
     if(currentlocation.length>0)
      {
             map.flyTo(
          {
            center: currentlocation
          })
      }
    
    
    //Updating Bot's Message
    document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov</br><br>You are now in the most important section</br></br>First start by clicking the polygon symbol </br>on the top left corner.</br>Now  mark your designated </br>corner locations</br></br>You can always refresh to startover"
    
    map.on('click', function (e) 
    {
        
             

      //Updating Bot's Message
      document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov</br><br>It seems you are on track</br></br> choose your corner locations</br></br>once you are done, double </br> click on the final location and click </br>the polygon symbol"
      var cornerlocation =[]
    

       //getting object version of lng and lat
       var lnglatObject = JSON.parse(JSON.stringify(e.lngLat.wrap()))
      
      // pushing cornerlocation and overall_corners global variable
       cornerlocation.push(lnglatObject.lng)
       cornerlocation.push(lnglatObject.lat)
       overall_corners.push([lnglatObject.lng,lnglatObject.lat])
    
       //Adding Marker to the selected Corner Location
        var marker = new mapboxgl.Marker
        ({
          color: "#7FFFD4",
          draggable: true
         })
        .setLngLat([cornerlocation[0], cornerlocation[1]])
        .addTo(map);
             
        let popup = new mapboxgl.Popup({offset:30})
        .setLngLat([cornerlocation[0], cornerlocation[1]])
        .setHTML('Lng:'+cornerlocation[0].toFixed(2)+' Lat:'+cornerlocation[1].toFixed(2))
        .addTo(map) 
         cornerRef.disabled= true
    })
    
    
    
      // This mapbox doubleclick was introduced to update Bot's Message
       map.on('dblclick', function (e) 
    {
      //Update Bot's Message
      document.getElementById('info').innerHTML="Automated Bot ilya kuvshinov</br><br>Congratulations! I'm glad that, my </br>guidance finally worked</br></br>now go and save the location</br></br>ciao" 
         
        cornerRef.disabled= true
    })
    
 
     //This is the newer version of Polygon system introduced by mapbox. Since it was coded on an Html page. We wanna press this to make it active
     var draw = new MapboxDraw
    ({
        displayControlsDefault: false,
        controls:
        {
        polygon: true,
        }
    });

     //Drawing the Polygon on map
     map.addControl(draw); 
    
}

//Defining Region List
var regionList = new RegionList();

//This function is responsible to reset the page
function resetRegion()
{
    
    //obtaining a confirmation from the user
    var confirmation = confirm("Are you sure you want to clear the region?");
   
      if(confirmation==true)
    {
      //erasing all in the array
      overall_corners=[];
    
      //It reloads the page
      document.location.assign("recordRegion.html")

    }
    
};




//This saves the region when the user clicks save Region Button
function saveRegion()
{
     console.log(overall_corners)
    
  if (overall_corners.length>=3)
  {
        //region Id as an empty array
        var regionId = "";
  
       //prompting the user to enter his region's nickname
       regionId = prompt("Enter your Region's Name/Nickname: ");
    
       //validating Inputs
          while(regionId == [])
       {
          alert("Enter a valid Region name")
          regionId = prompt("Please enter Region Name/Nickname: ");
       }
        
     //calling back the class Region with it's parametres
      var regionName = new Region(regionId,overall_corners,new Date());
    
     //getting into addRegion function that stays in regionList class
     regionList.addregion(regionName);
   
     console.log(regionList.numOfRegions())
     console.log(localStorage.getItem("RegionListnum"));
    
      //Redirects the page to index.html
      document.location.assign("index.html")
      cornerLocations = [];
  }
    
  else
  {
    alert("Select at least three corner locations")
   }
}


     


   


 



var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 

//getting cornerlocation coordinates from local storage
var regionDisplay= JSON.parse(localStorage.getItem("Regionlistnum"))
        
var span = JSON.parse(localStorage.getItem('FenceDifnew'))


console.log(regionIndex)
console.log(regionDisplay)

//Defining geod that is responsible in finding Polygon Area
let geod = GeographicLib.Geodesic.WGS84;

//Checking what we are getting from geod
console.log(geod)

//Defining coordinates of polygon under geodPolygonArea 
var geodPolygonArea = geod.Polygon(false), i, Coordinates

NewRegion = new Region()
NewRegionList = new RegionList()


//mapbox api access
mapboxgl.accessToken = 'pk.eyJ1IjoiZGVuaXNoYW45OSIsImEiOiJja2phOG9kZ3QwOGtiMzNtZWhsZ200cmN4In0.QwDeret4pB_wfZjRxI-50w';
let map = new mapboxgl.Map({
container: 'map', // container id
style: 'mapbox://styles/mapbox/streets-v11',
center: regionDisplay[regionIndex]._cornerLocations[1], // starting position
zoom: 11.8
    // starting zoom
});

// GeoJSON object to hold our measurement features
var geojson = {
    'type': 'FeatureCollection',
    'features': []
};
     
// Used to draw a line between points
var linestring = {
    'type': 'Feature',
    'geometry': {
        'type': 'LineString',
        'coordinates': []
    }
};
var polygon = {
    'type':'Feature',
    'geometry':{
        'type':'Polygon',
        'coordinates':[]
    }
};

// Event to load sources and layers of the map
map.on('load', function () {
    map.addSource('geojson', {
        'type': 'geojson',
        'data': geojson
    });

    // Add styles to the map
    // Layer to add points to the map
    map.addLayer({
        id: 'measure-points',
        type: 'circle',
        source: 'geojson',
        paint: {
            'circle-radius': 5,
            'circle-color': 'rgb(153,153,0)'
        },
        filter: ['in', '$type', 'Point']
    });
   


});
//checking what we will get from regionDisplay[regionIndex]._cornerLocations and adjusting the code according to that
console.log(regionDisplay[regionIndex]._cornerLocations)



//This loads the map and starts the function
   map.on('load', function () 
{       


    map.addSource('maine', 
 {
   'type': 'geojson',
   'data': {
   'type': 'Feature',
   'geometry': {
   'type': 'Polygon',
   'coordinates': [
    regionDisplay[regionIndex]._cornerLocations
    ]
    }
    }
 });
     

       
    map.addLayer
    ({
   'id': 'maine',
   'type': 'fill',
   'source': 'maine',
   'layout': {},
    'paint': {
              'fill-color': [
                'match',
                ['get', 'selected'],
                'true', '#64bdbb', // if selected true, paint in blue
                '#0000ff'] ,
    'fill-opacity': 0.6
              }
     })
})




//this function draws fences
function fences()
{
  NewRegion.fences()

}

//this function removes fences
function removeFence()
{
         
 document.location.assign("viewRegion.html")
    
}



    


//This function calculates the Area and perimeter inside the Polygon
 function calculateArea()
 {
    NewRegion.PolygonArea()
}






//This function deletes the Region that stored in local Storage and redirects back to the Index.html page
 function deleteRegion()
{
NewRegionList.removeregion()
}

//this function reloads settings page
function Settings(){
     document.location.assign("Settings.html")
}

//Header bar title with the following Region name
if (regionIndex !== null)
{
  document.getElementById("headerBarTitle").textContent = regionDisplay[regionIndex]._name +" (Fence Distance: " + span +")";
       
}

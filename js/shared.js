var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 

//getting cornerlocation coordinates from local storage
var regionDisplay= JSON.parse(localStorage.getItem("Regionlistnum"))

//Region Class that includes name, cornerlocations and time
class Region {
    constructor(name,cornerLocations,time) 
    {
        this._name = name
        this._cornerLocations = cornerLocations
        this._Datetime= time
    }
    //setting nick name
    set nickName(name)
    {
    this._nickName=name
    }
    get nickName()
    {
        return this._nickName
    }
    //setting cornerlocations
    set cornerlocations(cornerLocations)
    {
        this._cornerLocations=cornerLocations
    }
    get cornerlocation()
    {
        return this._cornerLocations
    }
    //setting date and time
    set dateTime(time)
    {
        this._dateTime = time
    }
    get dateTime()
    {
    return this._dateTime
    }
    //function fences draws the fence
    fences()
    {
        
        //Defining s12 values
        var s12 =[]

        //var azi1
        var azi1 =[]

        //Defining an empty array for coordinates
        var Coordinates = []; 

        //Pusing the coordinates those are inside the localstorage for the following index
        for( var i=0 ; i < regionDisplay[regionIndex]._cornerLocations.length;i++ )
         {
             Coordinates.push(regionDisplay[regionIndex]._cornerLocations[i])
             console.log(Coordinates)
         }
         //checking coordinates
         console.log(Coordinates)
        
         for (i = 0; i<Coordinates.length-1; ++i)
         {
         var DistanceDirection = geod.Inverse(Coordinates[i][1], Coordinates[i][0],Coordinates[(i)+1][1], Coordinates[(i)+1][0])
          console.log(DistanceDirection)
         s12.push(DistanceDirection.s12)
          azi1.push(DistanceDirection.azi1)
        }

         //adding the distance between starting and end
        var lastandfirst = geod.Inverse(Coordinates[Coordinates.length-1][1], Coordinates[Coordinates.length-1][0],Coordinates[0][1], Coordinates[0][0])

        //pushing start and end lnglat
        s12.push(lastandfirst.s12)
        azi1.push(lastandfirst.azi1)

        //calculating Total Distance
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        var distance= s12.reduce(reducer)
         console.log(distance)
         console.log(localStorage.getItem('FenceDifnew'))
         var fence_dif=4       
     var FenceDif = JSON.parse(localStorage.getItem('FenceDifnew'))

        if(FenceDif === null)
         {
         FenceDif=4
         }
      

        var FencePosts=[]
        localStorage.setItem('fences',JSON.stringify(numberOfFences))

        for( let m = 0; m < s12.length ; m++){
        FencePosts.push(Math.floor(s12[m] /FenceDif));
        }
        var numberOfFences = 0;
        
        //adding number of fences and calculating total
        for ( let z = 0; z < FencePosts.length; z++)
        {
          numberOfFences = numberOfFences + FencePosts[z];   
        }
        
        localStorage.setItem('fences',JSON.stringify(numberOfFences))

        var postCoordinates = [];
        //Loop to calculate the coordinates of the fence posts
        for(let n = 0; n < Coordinates.length; n++)
        {
           for( let m = 0 ; m < FencePosts[n] ; m++ )
           {
             var gap = FenceDif * (m + 1)
             var c = geod.Direct(Coordinates[n][1],Coordinates[n][0],azi1[n],gap);
             postCoordinates.push([c.lon2,c.lat2]);
           }
        }
        
        
        document.getElementById('fence').innerHTML =   '<li class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"  style="width: 300px; height: 110px; min-width: initial;color: #BCC6C; background:#FBB117 " '+
                '<span class="mdl-list__item-primary-content">' +
                '<span>' +"Distance Between Fences: "+FenceDif+" meter" +'</span>' +"     "+
                '<span class="mdl-list__item-sub-title">' +'</br></br>'+"Fences: " + numberOfFences + '</span>' +
                '</span>' +
                '</li>'
         
        
        //checking post coordinates
        console.log(postCoordinates)
        
        //This draws the fence
        for(let y = 0; y <postCoordinates.length; y++)
        {
               var point = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [postCoordinates[y][0],postCoordinates[y][1]]       
                },
          }
          geojson.features.push(point)
        }
           map.getSource('geojson').setData(geojson);


    }
    
    
    PolygonArea()
 {
     var Coordinates = []; 

    //Pusing the coordinates those are inside the localstorage for the following index
    for( var i=0 ; i < regionDisplay[regionIndex]._cornerLocations.length;i++ )
   {
      Coordinates.push(regionDisplay[regionIndex]._cornerLocations[i])
      console.log(Coordinates)
   }
             
    //getting all coordinates inside geodPolygonArea
     for (i = 0; i < Coordinates.length; ++i)
     geodPolygonArea.AddPoint(Coordinates[i][1], Coordinates[i][0]);
     geodPolygonArea = geodPolygonArea.Compute(false, true);
     
     //Checking the area
     console.log(geodPolygonArea)
     
     //Adding both Area and perimeter in html with blue box
    document.getElementById('info').innerHTML =   '<li class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"  style="width: 300px; height: 110px; min-width: initial;color: #BCC6C; background:#357EC7 " '+
                '<span class="mdl-list__item-primary-content">' +
                '<span>' +"Region Area: "+ Math.abs(geodPolygonArea.area.toFixed(1))+" square metres" + '</span>' +"     "+
                '<span class="mdl-list__item-sub-title">' +'</br></br>'+"Region Perimeter: " + Math.abs(geodPolygonArea.perimeter.toFixed(1))+" meter" + '</span>' +
                '</span>' +
                '</li>'
 }
}



//Region List Class that is responsible addRegion, The important part in this app.
class RegionList {
    
        constructor()
    {
        this._Regionlistnum=[]
     
    }

        getregionlist()
   {
 
     return this._Regionlistnum

  }

     numOfRegions()
     {
    
      return this._Regionlistnum.length
     }
    
      // add region
     addregion(name)
    {
             
            //pushing name under saved region given by monash
            this._Regionlistnum.push(name) 
            savedRegions=JSON.parse(localStorage.getItem("Regionlistnum"))
         
            //checking whether the region got saved
            console.log(savedRegions)
                                
           //saving region accordingly and validating it
            if( savedRegions !== null) 
            {
             savedRegions.push(name);
            localStorage.setItem("Regionlistnum", JSON.stringify(savedRegions));
                console.log(localStorage.getItem("Regionlistnum"))
            }
           else
           {
            localStorage.setItem("Regionlistnum", JSON.stringify(this._Regionlistnum)); 
             }
  }

     // get region

    getregion(id)
    {
             var Index = this._Regionlistnum.indexOf(id)
             return Index
    }
    

    // remove region

    removeregion()
    {
     //checking region index
    console.log(regionIndex)
        
    //deleting region using splice
    regionDisplay.splice(regionIndex,1);
    
    //deleting the region in local storage
    localStorage.setItem("Regionlistnum",JSON.stringify(regionDisplay))
    
    //getting back to index.html page
    document.location.assign("index.html")
        
    //checking whether the region got deleted
    console.log(regionDisplay)
   }
}


//This function changes the fence distance
function Changes()
{
    
     var FenceDif=document.getElementById('Value')
   
      difference = Number(FenceDif.value)
     console.log(difference)
     console.log(typeof(difference))
         

   

     localStorage.setItem('FenceDifnew',JSON.stringify(difference))
    
    
     var fence_dis = JSON.parse(localStorage.getItem('FenceDifnew'))
      if(fence_dis ===0|| fence_dis === null) 
      {
          alert("Enter a valid number")
         document.location.assign("Settings.html")
      }
    
    else
    {
         alert("Sucessfully Updated")
           document.location.assign("viewRegion.html")
             fence_dis= 4
        }

   
}



function defaultDif()
{
    //fence difference to default value
     var difference = 4
     
     //checking difference
    console.log(difference)
    localStorage.setItem('FenceDifnew',JSON.stringify(difference))
    
    alert("Sucessfully Updated")
    document.location.assign("ViewRegion.html")
  
}


// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];


document.getElementById('grid').innerHTML="</br></br>Automated Bot ilya kuvshinov</br><br></br>I'm Back</br></br>It seems something went wrong</br></br>Change your distance of the customized fence and press the Save Button"